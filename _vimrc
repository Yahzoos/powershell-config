" Johannes Kroll
" Neovim Config
"
" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

if has("gui_running")
    " GUI is running or is about to start.
    " Mazimize gvim window.
    set lines=999 columns=999
else
    " This is console Vim.
    if exists("+lines")
        set lines=50
    endif
    if exists("+columns")
        set columns=100
    endif
endif

set nocompatible
" Rebind <Leader> key
let g:mapleader = ","

" Bind nohl
"Removes highlight of your last search
 noremap <C-n> :nohl<CR>
 vnoremap <C-n> :nohl<CR>
 inoremap <C-n> :nohl<CR>

" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
" map <C-j> <C-w>j
" map <C-k> <C-w>k
" map <C-l> <C-w>l
" map <C-h> <C-w>h
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" easier moving between tabs
 map <Leader>n <esc>:tabprevious<CR>
 map <Leader>m <esc>:tabnext<CR>

" easier moving of code blocks
" Try to go into visual mode (v), then select several lines of code here and
" then press ``>`` several times.
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation

" Show whitespace
" MUST be inserted BEFORE the colorscheme command
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertLeave * match ExtraWhitespace /\s\+$/

" Color scheme
set termguicolors "comment out for duller colors
"color gruvbox
"set bg=dark
packadd! dracula
colorscheme dracula


" Enable syntax highlighting
 filetype off
 filetype plugin indent on
 syntax on

" Showing line numbers and length
 set number  " show line numbers
 set tw=79   " width of document (used by gd)
 set nowrap  " don't automatically wrap on load
 set fo-=t   " don't automatically wrap text when typing
" set colorcolumn=80
 highlight ColorColumn ctermbg=233

" easier formatting of paragraphs
 vmap Q gq
 nmap Q gqap

" Useful settings
 set history=700
 set undolevels=700

" Real programmers don't use TABs but spaces
 set tabstop=4
 set softtabstop=4
 set shiftwidth=4
 set shiftround
 set expandtab

" Make search case insensitive
 set hlsearch
 set incsearch
 set ignorecase
 set smartcase

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
 set nobackup
 set nowritebackup
 set noswapfile

" toggle spell checker
nnoremap <leader>s :set invspell<CR>

" copy and paste from windows
 map <Leader>c "*y
 map <Leader>p "*p 
