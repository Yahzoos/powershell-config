#Profile for Johannes Kroll

#Functions

Function Connect-GCCHTeams {Connect-MicrosoftTeams -TeamsEnvironmentName TeamsGCCH}
#Alias
if ($IsLinux) {echo "im on linux"}
else {
Set-Alias -Name npp -Value notepad++.exe
Set-Alias -Name vim -Value vim.exe
Set-Alias -Name gvim -Value gvim.exe
}
Set-Alias -Name ls -Value Get-ChildItemColorFormatWide -option AllScope
Set-Alias -Name exo -Value Connect-ExchangeOnline
Set-Alias -Name tms -Value Connect-MicrosoftTeams
Set-Alias -Name gtms -Value Connect-GCCHTeams

#Make PowerShell Usable
Set-PSReadlineOption -EditMode vi -BellStyle None -ViModeIndicator Cursor
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete
Get-ChildItemColor | Out-Null
$GetChildItemColorTable.File['Directory'] = "DarkCyan"
$Global:GetChildItemColorVerticalSpace = 0
